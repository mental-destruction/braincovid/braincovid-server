import urllib.request
import bs4
import json
import time


class Collector:
    def __init__(self, data):
        self.data = data

    def collect_data(self):
        i = self.__read_link("https://www.worldometers.info/coronavirus/").find(id="maincounter-wrap")
        val_str = str(i.find("span").get_text()).replace(",", "").strip()
        val = int(val_str)
        print("value: " + val_str)
        return val

    def __read_link(self, link):
        response = urllib.request.urlopen(link)
        return bs4.BeautifulSoup(response.read().decode(), "html.parser")

    def write_data(self, dat_input_value):
        with open(self.data, 'w', encoding='utf-8') as f:
            json.dump(dat_input_value, f, ensure_ascii=False, indent=4)


c = Collector("Dat/data.json")


def collector_thread_run(args=()):
    prev_time = time.time()
    print(prev_time)

    dat = {
        "Dat": c.collect_data(),
        "lastUpdate": time.time()
    }
    c.write_data(dat)
    print("Data recorded at " + time.ctime() + " | " + str(dat))

    while 1:
        cur_time = time.time()
        if prev_time + 10 < cur_time:
            prev_time = cur_time

            dat = {
                "Dat": c.collect_data(),
                "lastUpdate": time.time()
            }
            c.write_data(dat)
            print("Data recorded at " + time.ctime()+" | " + str(dat))



