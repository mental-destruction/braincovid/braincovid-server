from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse
import json
# import Web.sever_logic as logic

def read_cur_data():
    import json
    with open("Dat/data.json", "r") as read_file:
        data = json.load(read_file)
    return data["Dat"]

class RequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        parsed_path = urlparse(self.path)
        self.send_response(200)
        self.end_headers()
        self.wfile.write(json.dumps({
            'data_out': read_cur_data()
        }).encode())
        return

    def do_POST(self):
        content_len = int(self.headers.getheader('content-length'))
        post_body = self.rfile.read(content_len)
        data = json.loads(post_body)

        parsed_path = urlparse(self.path)
        self.send_response(200)
        self.end_headers()
        self.wfile.write(json.dumps({
            'method': self.command,
            'path': self.path,
            'real_path': parsed_path.query,
            'query': parsed_path.query,
            'request_version': self.request_version,
            'protocol_version': self.protocol_version,
            'body': data
        }).encode())
        return

def run():
    if __name__ == '__main__':
        server = HTTPServer(('localhost', 8000), RequestHandler)
        print('Starting server at http://localhost:8000')
        server.serve_forever()
run()